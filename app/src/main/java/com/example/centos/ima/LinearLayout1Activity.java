package com.example.centos.ima;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class LinearLayout1Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_linear_layout1);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("LinearLayout");
    }
}
