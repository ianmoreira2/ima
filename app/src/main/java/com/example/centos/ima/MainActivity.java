package com.example.centos.ima;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        findViewById(R.id.btn_example).setOnClickListener(this);
        findViewById(R.id.btn_linear).setOnClickListener(this);
        findViewById(R.id.btn_exibicao).setOnClickListener(this);
        findViewById(R.id.btn_orientation).setOnClickListener(this);
        findViewById(R.id.btn_scroll).setOnClickListener(this);
        findViewById(R.id.btn_gridlayout).setOnClickListener(this);
        findViewById(R.id.btn_webView).setOnClickListener(this);
        findViewById(R.id.btn_relative).setOnClickListener(this);
        findViewById(R.id.btn_recycler).setOnClickListener(this);

    }

    /*
        * Esses são os métodos ativados durante o ciclo de vida
        * de uma tela. Ao interagir com a aplicação observe no Logcat
        * os estados da sua tela principal.
        *
    */

    @Override
    protected void onStart() {
        super.onStart();
        Toast.makeText(this, "onStart()", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Toast.makeText(this, "onResume()", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Toast.makeText(this, "onPause()", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Toast.makeText(this, "onRestart()", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Toast.makeText(this, "onDestroy()", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View v) {
        Intent generic= null;

        switch (v.getId()){
            case R.id.btn_example:
//                    generic = new Intent(this, )
                break;
            case R.id.btn_exibicao:
                generic = new Intent(this, ExibicaoActivity.class);
                break;
            case R.id.btn_linear:
                generic = new Intent(this, LinearLayout1Activity.class);
                break;
            case R.id.btn_orientation:
                generic = new Intent(this, OrientationActivity.class);
                break;
            case R.id.btn_scroll:
                generic = new Intent(this, ScrollViewActivity.class);
                break;
            case R.id.btn_relative:
                generic = new Intent(this, RelativeActivity.class);
                break;
            case R.id.btn_webView:
                generic = new Intent(this, WebViewActivity.class);
                break;
            case R.id.btn_recycler:
                generic = new Intent(this, RecyclerViewActivity.class);
                break;
            case R.id.btn_gridlayout:
                generic = new Intent(this, GridLayoutActivity.class);
                break;
                default:
                    return;
        }
        startActivity(generic);
    }
}
